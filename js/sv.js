;(function($, window, document, undefined){
	/*
	 * GLOBAL DEFINITON
	 */
	var defaults = {
		colors : {
      "A":"#E4E430",
			"B":"#BCD56E",
			"C":"#73AAF0",
			"D":"#A0EE6E",
			"E":"#DD996E",
			"F":"#28A52E",
			"G":"#7F5194",
			"H":"#A8151E",
			"I":"#CCD56E",
			"K":"#12D56E",
			"L":"#88D566",
			"M":"#33D56E",
			"N":"#A85565",
			"P":"#BCD56C",
			"Q":"#AAA56E",
			"R":"#33C56E",
			"S":"#DDD51E",
			"T":"#AD0D2D",
			"U":"#CC2929",
			"V":"#D58894",
			"W":"#1212CC",
			"Y":"#AA1155",
			"Z":"#88D5EE",
			"*":"#555555"
	    },
	    highlightColor : "red"
	}

	var proteinTable = {
		"A":["GCT", "GCC", "GCA", "GCG",
			 "GCU"],
		"B":["AAC", "AAT", "GAC", "GAT",
			 "AAU", "GAU"],
		"C":["TGC", "TGT",
			 "UGC", "UGU"],
		"D":["GAC", "GAT"],
		"E":["GAA", "GAG"],
		"F":["TTT", "TTC", 
			 "UUU", "UUC"],
		"G":["GGA", "GGC", "GGG", "GGT"],
		"H":["CAT", "CAC",
			 "CAU"],
		"I":["ATT", "ATC", "ATA",
			 "AUU", "AUC", "AUA"],
		"K":["AAA", "AAG"],
		"L":["CTT", "CTC", "CTA", "CTG", "TTA", "TTG",
			 "CUU", "CUC", "CUA", "CUG", "UUA", "UUG"],
		"M":["ATG",
			 "AUG"],
		"N":["AAC", "AAT",
			 "AAU"],
		"P":["CCT", "CCC", "CCA", "CCG",
			 "CCU"],
		"Q":["CAA", "CAG"],
		"R":["AGA", "AGG", "CGA", "CGC", "CGG", "CGT",
			 "CGU"],
		"S":["AGC", "AGT", "TCA", "TCC", "TCG", "TCT",
			 "AGU", "UCA", "UCC", "UCG", "UCU"],
		"T":["ACT", "ACC", "ACA", "ACG",
			 "ACU"],
		"V":["GTT", "GTC", "GTA", "GTG",
			 "GUU", "GUC", "GUA", "GUG"],
		"W":["TGG",
			 "UGG"],
		"Y":["TAT", "TAC",
			 "UAU", "UAC"],
		"Z":["CAA", "CAG", "GAA", "GAG"],
		"*":["TAA", "TAG", "TGA",
			 "UAA", "UAG", "UGA"],
	}

	var tempOpen = 
	["<table class='table table-bordered _sv_table'>",
		"<tbody>"];

	var addFeatureModal = 
	['<div class="modal hide fade _svf_feature_label_modal">',
		'<div class="modal-header">',
			'<button type="button" class="close" data-dismiss="modal">x</button>',
			'<h3>Add new sequence feature</h3>',
		'</div>',
		'<div class="modal-body">',
			'<div class="control-group">',
				'<label class="control-label" style="float:left; margin:4px 10px; 4px; 4px">Feature label</label>',
				'<div class="controls">',
					'<input type="text" class="_svf_feature_label_input"/>',
					'<span class="help-inline" style="position:relative; top:-3px;"></span>',
				'</div>',
			'</div>',
		'</div>',
		'<div class="modal-footer">',
			'<a href="#" class="_svf_submit_feature btn btn-primary">Save</a>',
			'<a href="#" class="btn" data-dismiss="modal">Close</a>',
		'</div>',
	'</div>'];

	var toolbar = 
	["<tr>",
		"<td colspan='2' class='_sv_toolbar'>",
			"<div class='_sv_motif_search input-append pull-right'>",
				"<span class='_sv_status'></span>",
				"<input type='text' class='_sv_search_box' placeholder='type here, for example : AT/GG/C/ATT'/>",
				"<span class='add-on'><i class='icon-search'></i></span>",
			"</div>",
			"<div class='input-prepend pull-left'>",
				"<span class='add-on'>Current sequence index </span>",
				"<input type='number' class='_sv_seq_index' value='1'/>",
			"</div>",
			"<div class='input-prepend pull-left _msv_active_row'>",
				"<span class='add-on'>Current Row </span>",
				"<input type='number' class='_sv_row_index' value='1'/>",
			"</div>",
			"<div class='pull-left'>",
				"<a class='_sv_translator btn btn-primary btn-mini'>Show translated protein form</a>",
				"<a class='_svf_add_feature btn btn-danger btn-mini'>Add new feature</a>",
				"<a class='_svf_save_feature btn btn-success btn-mini' style='display:none'>Save</a>",
				"<a class='_svf_cancel_feature btn btn-danger btn-mini' style='display:none'>Cancel</a>",
				"<a class='_sv_alignment btn btn-info btn-mini'>Align sequence</a>",
			"</div>",
		"</td>",
	"</tr>"];

	var contentBar = 
	["<tr>",
		"<td width='30%' class='_sv_info_container'>",
		"</td>",
		"<td class='_sv_sequence_container_outer'>",
			"<div class='_sv_sequence_container'></div>",
			"<div class='_sv_progress_container'>",
				"<div class='_sv_progress_label'>",
					"<i>Loading sequence viewer</i>",
					"<div class='_sv_progress_percent'>0%</div>",
				"</div>",
			"</div>",
		"</td>",
	"</tr>"];

	var midBar = 
	["<tr class='_msv_midbar'>",
		"<td width='30%' style='padding:5px;'>",
				"<div class='input-append pull-left'>",
					"<input type='text' class='_msv_info_box'/>",
					"<span class='add-on'><i class='icon-search'></i></span>",
				"</div>",
		"</td>",
		"<td class='_msv_equality_container_outer'>",
			"<div class='_msv_equality_container'></div>",
		"</td>",
	"</tr>"];

	var tempClose =
		["</tbody>",
	"</table>"]

	var loadingSequenceTemplate = "<center style='padding:7px;'><i style='color:#999'>Loading sequence viewer..</i></center>";

	/*
	 * Translate Logical Operator to
	 * Correct Regular Expression
	 * for example AT/GG/C/ATT => A[TG][GCA]TT
	 */
	function translateLogicalOperator(query) {
		var last;
		var markers = [];
		var markerClosed = true;
		if (query.length < 3) return query;
		for (i=0; i < query.length; i++) {
			if (query[i] == '/') {
				// Start Marking rules
				if (markerClosed) {
					markers.push({
						from:i-1,   
						to:-1
					});
					markerClosed = false;
				}
				// Stop Marking rule
				if (query.charAt(i+2) != '/') { // close the current markers
					markers[markers.length-1].to = i+1;
					markerClosed = true; 
				}
			}
		}
		// close the uncompleted marking
		if (!markerClosed) markers[markers.length-1].to = markers.length;

		var result = "",
			lastFrom = 0;
		// add square brakets for valid regular expression
		for (i=0; i<markers.length; i++) {
			var from = markers[i].from,
				to = markers[i].to;
			result += query.substring(lastFrom, from) + "[" + query.substring(from, to+1) + "]";
			lastFrom = to+1;
		}
		// append content after the last stop marker
		result += query.substring(lastFrom, query.length);
		return markers.length == 0 ? query : result.replace(/[\/]+/g, "");
	}

	/*
	 * Translate search query into specified rules
	 * @return RegExp
	 */
	function translateSearchQuery(query) {
		if (!this.queryList) this.queryList = []; // query cache
		query = translateLogicalOperator(query);

		// handle the invalid search query
		try {
			if (!this.queryList[query]) this.queryList[query] = new RegExp(query, "g");
			return this.queryList[query];
		} catch(e) {
			return false;
		}
	}

	/*
	 * Translate a nucleotide sequence to protein form
	 * @return String
	 */
	function translateToProtein(data) {
		var result = "";
		for (var i=0; i<data.length; i=i+3) {
			var codon = data.substr(i,3);
			if (/[- ]+/.test(codon)) {
				result += "-";
			} else {
				var matchedName = "";
				for (var name in proteinTable) {
					var protein = proteinTable[name];
					for (j=0; j < protein.length; j++) {
						if (protein[j] == codon) {
							matchedName = name;
							break;
						}
					}
					if (matchedName) break;
				}
				result += matchedName;
			}
		}
		return result;
	}

	/*
	 * parse fasta file and return list of sequence object
	 * return [seq1, seq2, .. seqn]
	 * seq object format :
	 * {
	 *   info : sequence info,
	 *   data : sequence data
	 * }
	 */
	function parseFasta(content) {
		var seqs = [];
		var lines = content.split("\n");
    var currentSequenceIndex = -1;
    var maxLength = 0;
    if (lines.length == 0) throw "Empty fasta file";
    for (i=0; i<lines.length; i++) {
    	if (lines[i].length == 0) continue;
    	if (lines[i][0] == ">") { // sequence info
    		if (seqs[currentSequenceIndex]) {
    			var data = seqs[currentSequenceIndex].data;
    			if (data.length > maxLength) maxLength = data.length;
    		}
    		currentSequenceIndex++;
    		seqs[currentSequenceIndex] = {
    			info : "",
    			data : ""
    		}
    		seqs[currentSequenceIndex].info = lines[i].substr(1, lines[i].length-1);
    	} else { // sequence data
    		seqs[currentSequenceIndex].data += i == lines.length-1 ? lines[i].substr(0, lines[i].length) : lines[i].substr(0, lines[i].length-1);
    	}
    }
    if (seqs[currentSequenceIndex]) {
			var data = seqs[currentSequenceIndex].data;
			if (data.length > maxLength) maxLength = data.length;
		}
    seqs.maxLength = maxLength;
    return seqs;
	}
	/*
	 * /GLOBAL DEFINITON
	 */


	/*
	 * SINGLE SEQUENCE VIEWER
	 */
	var singleViewerName = "singleSequenceViewer";
	function SingleSequenceViewer(element, options) {
		this._name = singleViewerName;
		this.element = element;
		this._defaults = defaults;
		
		// Feature Management
		this.addFeatureMode = false;
		this.features = [];
    this.addFeatureBuffer = {
    	from:-1,
    	to:-1,
    	label:""
    }
    this.editFeatureBuffer = {
      from:-1,
      to:-1,
      label:"",
      id:""
    }
    this._defaults.onAddFeature = function(feature){};
    this._defaults.onRemoveFeature = function(feature){};
		this._defaults.onEditFeature = function(feature){};


		this.options = $.extend({}, this._defaults, options);
		this.options.colors = $.extend({}, this._defaults.colors, options.colors);
		this.features = this.options.features;
		
		this.seq = { // sequence object
    	info:"",
    	data:""
    }
    this.seqBackup = {
    	info:"",
    	data:""
    }
    this.proteinMode = false;
    this.firstSearch = true; 
    this.highlightPosition = {
    	from:-1,
    	to:-1,
    	row:-1
    }
    this.viewerTemplate = tempOpen.concat(
			addFeatureModal.concat(
			toolbar.concat(
			contentBar.concat(
	  tempClose))));
    this.viewerTemplate = this.viewerTemplate.join("");
    this.sequenceIndexInput = null;
    this.rowIndexInput = null;
		this.init();
	}

	/*
     * Reset state of the viewer
     */
	SingleSequenceViewer.prototype.resetState = function(){
		this.highlightPosition = {
    	from:-1,
    	to:-1,
    	row:-1
    }
    this.notifySequenceIndex(1);
    this.notify("");
    this.firstSearch = true;
    $(this.element).find("._sv_sequence_container").scrollLeft(0);
    this.loadSequenceData();
    if (this._name == singleViewerName) this.loadFeature();
	}

	/*
     * Set the viewer status
     */
	SingleSequenceViewer.prototype.notify = function(msg, duration) {
		var plugin = this;
    $(this.element).find("._sv_status").html(msg);
    if (duration) {
    	setTimeout(function(){
				$(plugin.element).find("._sv_status").fadeOut(1000, function(){
					plugin.notify("");
					$(this).show();
				});
			}, duration);
    }
  }

    /*
     * Notify the current sequence index
     */
	SingleSequenceViewer.prototype.notifySequenceIndex = function(index) {
    this.sequenceIndexInput.val(index);
	}

	/*
   * Notify progress of some process
   */
  SingleSequenceViewer.prototype.notifyProgress = function(percent){
  	var plugin = this;
  	if (percent == 100) {
  		$(plugin.element).find("._sv_progress_percent").html("100%");
  		$(plugin.element).find("._sv_progress_container").fadeOut(500, function(){
  			$(plugin.element).find("._sv_progress_percent").html("0%");
  			$(plugin.element).find("._sv_sequence_container")
  											 .removeClass("_sv_absolute_container")
  											 .show();
  		});
  		return;
  	}
  	$(this.element).find("._sv_progress_percent").html(percent+"%");
  }

	/*
	 * single sequence fasta file parser
	 * fill the sequence object with the parsed fasta file content
	 */
	SingleSequenceViewer.prototype.parseFastaFile = function(fasta) {
  	var seqs = parseFasta(fasta);
  	this.seq = seqs[0];
  }

    /*
	 * Create container for the sequence viewer
	 * with default tools
	 */
	SingleSequenceViewer.prototype.createContainer = function() {
		this.element.innerHTML = this.viewerTemplate;
		$(this.element).find("._sv_progress_container").hide();
		$(this.element).find("._msv_active_row, ._sv_alignment").remove();
		$(this.element).find("._sv_info_container").css({
			'min-height':'100px'
		});
		this.sequenceIndexInput = $(this.element).find("._sv_seq_index");
		this.rowIndexInput = $(this.element).find("._sv_row_index");
	}

	/*
	 * Load sequence data to the container
	 */
	SingleSequenceViewer.prototype.loadSequenceData = function() {
		colorListSingle = this.options.colors;
		var seqColorSingle = function(d) {
    	if (colorListSingle[d]) return colorListSingle[d];
    }
    var plugin = this;

    var $progress = $(this.element).find("._sv_progress_container");
    $progress.width($progress.parent().width());

    $(this.element).find("._sv_info_container").html(this.seq.info);

		var $container = $(this.element).find("._sv_sequence_container, ._sv_progress_container");
		$container.width($container.parent().width());
		$container.height($container.parent().height());
		$(this.element).find("._sv_sequence_container").addClass("_sv_absolute_container").hide();
		$(this.element).find("._sv_progress_container").show();

		var container = $container.get(0);
		this.dataContainer = d3.select(container).text("")
			.append("svg")
			.attr("width", (30 * this.seq.data.length)+"px")
			.attr("height", "50px");

		// sequence box
		this.dataContainer.selectAll("rect")
			.data(this.seq.data)
			.enter()
			.append("rect")
			  .attr("class", function(d,j){ return "_sv_box _sv_seq_" + j; })
              .attr("x", function(d,j){ return j*30; })
              .attr("y",0)
              .attr("width", 30)
              .attr("height", 30)
              .style("fill", function(d,i){ return seqColorSingle(d); })
              .attr("stroke", "#000");
    
  	// sequence label
		this.dataContainer.selectAll("._sv_seq_label")
      .data(this.seq.data)
      .enter()
        .append("text")
          .attr("class", function(d,j){ return "_sv_seq_label _sv_seq_label_"+j})
          .attr("transform", function(d,j) {
            return "translate ("+((j*30)+10)+", 20)";
          })
          .each(function(d,i){
            d3.select(this).text(d);
          });

    // sequence index
		this.dataContainer.selectAll("._sv_seq_index")
      .data(this.seq.data)
      .enter()
        .append("text")
          .attr("class", "_sv_seq_index")
          .attr("transform", function(d,j) {
          	var percent = Math.round(plugin.seq.data.length/(j+1)*100);
          	plugin.notifyProgress(percent);
            return "translate ("+((j*30)+2)+", 28)";
          })
          .each(function(d,i){
            d3.select(this).text(i+1);
          });
  	this.bindSequenceDataEvents();
  	if (this.addFeatureMode) {
  		this.highlight(0, this.seq.data.length);
  	}
	}

	/*
	 * Highlight the sequence at specified index
	 */
	SingleSequenceViewer.prototype.highlight = function(from, to) {
		var container = $(this.dataContainer[0][0]);
		var highlighter = "";
		// Reset highlight
		if (this.highlightPosition.from != -1 || this.highlightPosition.to != -1) {
			this.resetHighlight();
		}
		for (i=from; i<to; i++) {
			var seq = $(this.element).find("._sv_seq_"+i);
			seq.get(0).oldFill = seq.css("fill");
			seq.attr("stroke", this.options.highlightColor)
			   .css("fill", this.options.highlightColor);
		}
		// Remember the highligthed index
		this.highlightPosition.from = from;
		this.highlightPosition.to = to;
	}

	/*
	 * unhighlight sequence with specified index
	 */
	SingleSequenceViewer.prototype.unhighlight = function(index) {
		var seq = $(this.element).find("._sv_seq_"+index);
		seq.attr("stroke", "black")
			 .css("fill", seq.get(0).oldFill);
	}

	/*
	 * Reset sequence highlight
	 */
	SingleSequenceViewer.prototype.resetHighlight = function() {
		for (var i=this.highlightPosition.from; i<this.highlightPosition.to; i++) {
			this.unhighlight(i);
		}
		this.highlightPosition.from = -1;
		this.highlightPosition.to = -1;

	}

	/*
	 * Motif Search Utility
	 *----------------------
	 * search types :
	 * - exactly match
	 * - search with logical operator, for example : AT/G/U => AT, AG, AU
	 */
	SingleSequenceViewer.prototype.motifSearch = function(query) {
		var q = query.toUpperCase();
		q = translateSearchQuery(q); // translate query into RegExp
		if (this.firstSearch) {
			this.firstSearch = false;
		} else {
			q.lastIndex = this.getCurrentSequenceIndex();
		}
		if (q.lastIndex >= this.seq.data.length-1) q.lastIndex = 0; // reset to the beginning

		// handle the invalid search query
		try {
			var result = q.exec(this.seq.data);
		} catch(e) {
			this.resetHighlight();
			this.notify("<i>Search not found for '" + query + "'</i>");
			return false; 
		}

		// search decission
		if (!result) { // Not Found
			q.lastIndex = 0; // Reset to first index of the sequence
			result = q.exec(this.seq.data);
			if (!result) { // Not Found at first index
				this.resetHighlight();
				this.notify("<i>Search not found for '" + query + "'</i>");
				return false;
			} else { // Found at first index
				this.highlight(result.index, result.index + result[0].length);
				this.notify("<i>Found at "+ (result.index+1) + " </i>");
			}
		} else { // Search found
			this.highlight(result.index, result.index + result[0].length);
			this.notify("<i>Found at "+ (result.index+1) + " </i>");
		}

		q.lastIndex = result.index + result[0].length;
		$(this.element).find("._sv_sequence_container").scrollLeft(result.index * 30);
		this.notifySequenceIndex(result.index+1);
	}

	/*
   * Get current sequence index from the view
   */
  SingleSequenceViewer.prototype.getCurrentSequenceIndex = function(){
  	var index = this.sequenceIndexInput.val();
		if (isNaN(index)) return 0;
		return index;
  }

	/*
	 * Go to specified index
	 */
	SingleSequenceViewer.prototype.goTo = function(specifiedIndex) {
		this.currentIndex = specifiedIndex;
		$(this.element).find("._sv_sequence_container").scrollLeft(specifiedIndex * 30);
	}

	/*
	 * Load sequence data to the container
	 */
	SingleSequenceViewer.prototype.generateToolsAction = function() {
		// Fix the context problem
		var plugin = this;
		
		// Search box
		$(this.element).find("._sv_search_box").keyup(function(e){
			if ($(this).val() == "") {
				plugin.notify("");
				plugin.resetHighlight();
				return;
			}
			if (e.keyCode == 13) {
				plugin.motifSearch($(this).val());
			}
		});

		// Active sequence index
		var onActiveSequenceIndexChanged = function(){
			var index = plugin.getCurrentSequenceIndex();
			plugin.firstSearch = false;
			plugin.goTo(index-1);
		};
		$(this.element).find("._sv_seq_index")
					   .keyup(onActiveSequenceIndexChanged)
					   .change(onActiveSequenceIndexChanged);

		// Sequence translation
		$(this.element).find("._sv_translator").click(function(){
      $(this.element).find("._svf_cancel_feature").click();
			$(this).addClass("disabled");
      if (plugin.proteinMode) {
        plugin.proteinMode = !plugin.proteinMode;
				plugin.seq.data = plugin.seqBackup;
				plugin.resetState();
				$(this).html("Show translated protein form")
					   .addClass("btn-primary")
					   .removeClass("btn-info");
			} else {
        plugin.proteinMode = !plugin.proteinMode;
				plugin.seqBackup = plugin.seq.data;
				plugin.seq.data = translateToProtein(plugin.seq.data);
				plugin.resetState();
				$(this).html("Show nucleotide form")
					   .addClass("btn-info")
					   .removeClass("btn-primary");
			}
			$(this).removeClass("disabled");
		});

		// Responsive feature
		var oldSingleWidth = $(window).width();
		$(window).resize(function(){
			if (oldSingleWidth == $(window).width()) return;
			var $container = $(plugin.element).find("._sv_sequence_container, ._sv_progress_container");
			$container.width($container.parent().width());
			$container.height($container.parent().height());
		});

    // Hide feature popover when container was scrolled
    $(this.element).find("._sv_sequence_container").scroll(function(){
      $(plugin.element).find("._svf_shadow_popover, .popover").remove();
    }); 
		plugin.bindAddFeatureEvents();
	}

	SingleSequenceViewer.prototype.bindAddFeatureEvents = function(){
		var plugin = this;
		$(this.element).find("._svf_add_feature").click(function(){
			plugin.addFeatureMode = true;
      $(plugin.element).find("._svf_shadow_popover, .popover").remove();
			$(this).hide();
			$(plugin.element).find("._svf_cancel_feature,._svf_save_feature").show();
			plugin.highlight(0, plugin.seq.data.length);
			plugin.notify("<font color='red'>Select starting index of feature</font>");
		});

		$(this.element).find("._svf_cancel_feature").click(function(){
			plugin.addFeatureMode = false;
			plugin.addFeatureBuffer = {
	    	from:-1,
	    	to:-1,
	    	label:""
	    };
			$(plugin.element).find("._svf_cancel_feature,._svf_save_feature").hide();
			$(plugin.element).find("._svf_add_feature").show();
			plugin.resetHighlight();
			plugin.notify("");
		});

		$(this.element).find("._svf_save_feature").click(function(){
      var $modal = $(plugin.element).find("._svf_feature_label_modal");
      if (plugin.addFeatureMode) {
        if (plugin.addFeatureBuffer.from == -1 ||
          plugin.addFeatureBuffer.to == -1) {
          plugin.notify("<font color='red'>Please select feature indices to add feature</font>");
          return;
        }
        var modalHeader = "<h3>Add new sequence feature</h3>";
      } else if (plugin.editFeatureMode) {
        if (plugin.editFeatureBuffer.from == -1 ||
          plugin.editFeatureBuffer.to == -1) {
          plugin.notify("<font color='red'>Please select feature indices to save feature</font>");
          return;
        }
        var modalHeader = "<h3>Edit sequence feature</h3>";
        $modal.find("._svf_feature_label_input").val(plugin.editFeatureBuffer.label);
      }
      $modal.find(".modal-header").html(modalHeader);
      $modal.on('shown', function(){
				$(plugin.element).find("._svf_feature_label_input").focus();
			}).on("show", function(){
				$(plugin.element).find("._svf_feature_label_input")
				.parent().parent() // control-group
				.removeClass("error")
				.find(".help-inline").html("");
			}).modal("show");
			plugin.notify("");
		});

		$(this.element).find("._svf_submit_feature").click(function(){
			var $label = $(plugin.element).find("._svf_feature_label_input");
			if ($label.val() == "") {
				$label.parent().parent()
							.addClass("error")
							.find(".help-inline").html("Cannot be blank");

				return false;
			}
      if (plugin.addFeatureMode) plugin.addFeatureBuffer.label = $label.val();
      if (plugin.editFeatureMode) plugin.editFeatureBuffer.label = $label.val();
			$(plugin.element).find("._svf_cancel_feature,._svf_save_feature").hide();
			$(plugin.element).find("._svf_add_feature").show();
			if (plugin.proteinMode) {
        if (plugin.addFeatureMode) {
          plugin.addFeatureBuffer.from *= 3;
          plugin.addFeatureBuffer.to = (plugin.addFeatureBuffer.to+1) * 3;
        } else if (plugin.editFeatureMode) {
           plugin.editFeatureBuffer.from *= 3;
          plugin.editFeatureBuffer.to = (plugin.editFeatureBuffer.to+1) * 3;         
        }
			}
      if (plugin.addFeatureMode) {
        if (plugin.options.onAddFeature(plugin.addFeatureBuffer, plugin) === false) return false;
        plugin.addFeatureMode = false;
        plugin.addFeatureBuffer = {
          from:-1,
          to:-1,
          label:""
        };
      } else if (plugin.editFeatureMode) {
        if (plugin.options.onEditFeature(plugin.editFeatureBuffer, plugin) === false) return false;
        plugin.editFeatureMode = false;
        plugin.editFeatureBuffer = {
          from:-1,
          to:-1,
          label:"",
          id:""
        };
      }
			$label.val("");
			plugin.resetHighlight();
			$(plugin.element).find("._svf_feature_label_modal").modal("hide");
		});

		$(plugin.element).find("._svf_feature_label_input").keyup(function(e){
			if (e.keyCode == 13) {
				$(plugin.element).find("._svf_submit_feature").click();
			}
		});
	}

	SingleSequenceViewer.prototype.bindSequenceDataEvents = function(){
		var plugin = this;
		for (var i=0; i<plugin.seq.data.length; i++) {
			$(plugin.element).find("._sv_seq_"+i+",._sv_seq_label_"+i)
			.data("sequence-index", i)
			.click(function(){
				if (plugin.addFeatureMode) {
					var index = $(this).data("sequence-index");
          if (!plugin.validateFeatureIndex(index)) { // validate feature index existence
            return false;
          }
					if (plugin.addFeatureBuffer.from == -1) {
						plugin.addFeatureBuffer.from = index;
						plugin.unhighlight(index);
						plugin.notify("<font color='red'>Select finishing index of feature</font>");
					} else if (plugin.addFeatureBuffer.to == -1) {
						if (index < plugin.addFeatureBuffer.from) {
              if (!plugin.validateFeatureIndex(index, plugin.addFeatureBuffer.from)) {
                return false;
              }
							var tmp = plugin.addFeatureBuffer.from;
							plugin.addFeatureBuffer.from = index;
							plugin.addFeatureBuffer.to = tmp;
						} else {
              if (!plugin.validateFeatureIndex(plugin.addFeatureBuffer.from, index)) {
                return false;
              }
							plugin.addFeatureBuffer.to = index;
						}
						for (var ix = plugin.addFeatureBuffer.from; ix<=plugin.addFeatureBuffer.to; ix++) {
							plugin.unhighlight(ix);
						}
						plugin.notify("<font color='green'>Save feature or select another starting index</font>");
					} else {
						plugin.highlight(0, plugin.seq.data.length);
						plugin.unhighlight(index);
						plugin.addFeatureBuffer.from = index;
						plugin.addFeatureBuffer.to = -1;
						plugin.notify("<font color='red'>Select finishing index of feature</font>");
					}
				} else if (plugin.editFeatureMode) {
          var index = $(this).data("sequence-index");
          if (!plugin.validateFeatureIndex(index)) { // validate feature index existence
            return false;
          }
          if (plugin.editFeatureBuffer.from == -1) {
            for (var uindex = plugin.editFeatureBuffer.from; uindex<=plugin.editFeatureBuffer.to; uindex++) {
              plugin.highlight(uindex);
            }
            plugin.editFeatureBuffer.from = index;
            plugin.unhighlight(index);
            plugin.notify("<font color='red'>Select finishing index of feature</font>");
          } else if (plugin.editFeatureBuffer.to == -1) {
            if (index < plugin.editFeatureBuffer.from) {
              if (!plugin.validateFeatureIndex(index, plugin.editFeatureBuffer.from)) {
                return false;
              }
              var tmp = plugin.editFeatureBuffer.from;
              plugin.editFeatureBuffer.from = index;
              plugin.editFeatureBuffer.to = tmp;
            } else {
              if (!plugin.validateFeatureIndex(plugin.editFeatureBuffer.from, index)) {
                return false;
              }
              plugin.editFeatureBuffer.to = index;
            }
            for (var ix = plugin.editFeatureBuffer.from; ix<=plugin.editFeatureBuffer.to; ix++) {
              plugin.unhighlight(ix);
            }
            plugin.notify("<font color='green'>Save feature or select another starting index</font>");
          } else {
            plugin.highlight(0, plugin.seq.data.length);
            plugin.unhighlight(index);
            plugin.editFeatureBuffer.from = index;
            plugin.editFeatureBuffer.to = -1;
            plugin.notify("<font color='red'>Select finishing index of feature</font>");
          }
        }
			});
		}
	}

  SingleSequenceViewer.prototype.validateFeatureIndex = function(start, end) {
    if (end === undefined) end = start;
    if (this.proteinMode) {
      start *= 3;
      end *= 3;
    }
    for (var featureIndex = start; featureIndex<=end; featureIndex++) {
      for (var index in this.features) {
        if (featureIndex >= this.features[index].from && featureIndex <= this.features[index].to) {
          return false;
        }
      }
    }
    return true;
  }

	/*
	 * Load feature from viewer options
	 */
	SingleSequenceViewer.prototype.loadFeature = function(){
		// feature line
    var container = this.dataContainer[0][0].parentNode;
    $(container).find("._svf_line, ._svf_desc_box, ._svf_shadow_popover").remove();
    $(container).append("<div class='_svf_shadow_popover'></div>");
    var $featurePopover = $(container).find("._svf_shadow_popover")
                                      .css("background-color", this.options.highlightColor);

    var plugin = this;                           
		this.dataContainer.selectAll("._svf_line")
			.data(this.features)
			.enter()
			.append("polyline")
			  .attr("class", function(d,j){ return "_svf_line _svf_line" + d.id; })
        .attr("points", function(d,j){
          var x1 = (d.from)*30+15,
              x2 = (d.to+1)*30-15;
          if (plugin.proteinMode) {
            if (d.from === d.to) {
              x2 = Math.ceil(d.from/3);
              x1 = x2-1;
            } else {
              x1 = Math.ceil(d.from/3);
              x2 = Math.ceil(d.to/3);
            }
            x1 = (x1)*30+15,
            x2 = (x2)*30-15;
          }
          return x1 + "," + 31 + " " +
                 x1 + "," + 40 + " " +
                 x2 + "," + 40 + " " +
                 x2 + "," + 31; 
        });
    // feature description
    this.dataContainer.selectAll("._svf_desc_box")
      .data(this.features)
      .enter()
      .append("polygon")
        .attr("class", function(d,j){ return "_svf_desc_box _svf_desc_box" + d.id; })
        .attr("data-feature-id", function(d,j){ return d.id; })
        .attr("data-feature-index", function(d,j){ return j; })
        .attr("rel", "popover")
        .attr("data-content", "It's so simple to create a tooltop for my website!")
        .attr("data-original-title", "Twitter Bootstrap Popover")
        .attr("points", function(d,j){
          var x1 = (d.from)*30+25,
              x2 = (d.to+1)*30-25;
          if (plugin.proteinMode) {
            if (d.from === d.to) {
              x2 = Math.ceil(d.from/3);
              x1 = x2-1;
              x1 = (x1)*30+5;
              x2 = (x2)*30-5;
            } else {
              x1 = Math.ceil(d.from/3);
              x2 = Math.ceil(d.to/3);
              x1 = (x1)*30+20;
              x2 = (x2)*30-20;
            }
          }
          return x1 + "," + 40 + " " +
                 x1 + "," + 50 + " " +
                 x2 + "," + 50 + " " +
                 x2 + "," + 40; 
        });

    var plugin = this;
    $(container).find("._svf_desc_box").mouseover(function(){
      if (plugin.addFeatureMode) return false;
      var index = $(this).attr("data-feature-index");
      var feature = plugin.features[index];
      $(container).find(".popover").remove();
      $featurePopover.remove();
      $(container).append("<div class='_svf_shadow_popover'></div>");
      $featurePopover = $(container).find("._svf_shadow_popover")
                                    .css("background-color", plugin.options.highlightColor);
      var popoverWidth = (feature.to-feature.from)*30-20;
      var leftOffset = (feature.from+1)*30-5;
      if (plugin.proteinMode) {
        popoverWidth = Math.ceil((feature.to-feature.from)/3+1)*30-70;
        leftOffset = (Math.ceil(feature.from/3)+1)*30-10;
      }
      if (popoverWidth <= 0) {
        popoverWidth = 20;
        leftOffset = plugin.proteinMode ? leftOffset-45 : leftOffset-20;
      }
      var currentScrollStartLimit = $(plugin.element).find("._sv_sequence_container").scrollLeft();
      currentScrollEndLimit = currentScrollStartLimit + $(container).width() - 100;
      currentScrollStartLimit = currentScrollStartLimit + 100;
      
      if (leftOffset > currentScrollEndLimit || leftOffset < currentScrollStartLimit) {
        var leftScroll = plugin.proteinMode ? Math.ceil(feature.from/3) : feature.from;
        $(plugin.element).find("._sv_sequence_container").scrollLeft(leftScroll*30);
      }
      $featurePopover.css({
        width : popoverWidth + "px",
        left : leftOffset + "px"
      });
      $(container).find(".popover").hide();
      $featurePopover.popover({
        placement:"bottom",
        title:feature.label,
        html:true,
        content:"<a class='_svf_edit' href='#'>Edit</a> | <a class='_svf_remove' href='#'>Remove</a>"
      }).click();
      $(container).click(function(e){
        if ($(e.target).hasClass("_svf_shadow_popover")) return false;
        $(container).find("._svf_shadow_popover").remove();
        $(container).find(".popover").hide();
      });

      // REMOVE FEATURE
      $(container).find("._svf_remove").unbind("click").click(function(){
        plugin.options.onRemoveFeature(feature, plugin);
        $(container).find(".popover").hide();
        return false;
      });

      // EDIT FEATURE
      $(container).find("._svf_edit").unbind("click").click(function(){
        plugin.editFeatureMode = true;
        plugin.editFeatureBuffer = feature;
        $(plugin.element).find("._svf_shadow_popover, .popover").remove();
        $(this).hide();
        $(plugin.element).find("._svf_cancel_feature,._svf_save_feature").show();
        plugin.highlight(0, plugin.seq.data.length);
        var highlightStart = feature.from,
            highlightEnd = feature.to;
        if (plugin.proteinMode) {
          highlightStart = Math.ceil(highlightStart/3);
          highlightEnd = Math.ceil(highlightEnd/3)-1;
        }
        for (var index = highlightStart; index <= highlightEnd; index++) {
          plugin.unhighlight(index);
        }
        plugin.notify("<font color='red'>Edit feature position</font>");
        //plugin.options.onEditFeature(feature, plugin);
        //$(container).find(".popover").hide();
        return false;
      });
    });
	}

  SingleSequenceViewer.prototype.addFeature = function(feature){
    this.features.push(feature);
  }

  SingleSequenceViewer.prototype.editFeature = function(feature){
    for (var index=0; index<this.features.length; index++) {
        if (this.features[index].id == feature.id) {
          this.features[index] = feature;
          break;
        }
    }
  }

  SingleSequenceViewer.prototype.removeFeature = function(id){
    for (var index in this.features) {
      if (this.features[index].id == id) {
        this.features.splice(index, 1);
      }
    }
  }

	SingleSequenceViewer.prototype.init = function(){
		var fastaFile = this.options["fasta"];
		if (!fastaFile) return this.element;

		var plugin = this;
		plugin.createContainer();
		$.ajax({
			"url":fastaFile,
			"dataType":"text",
			success:function(fasta){
				plugin.parseFastaFile(fasta);
				plugin.loadSequenceData();
				plugin.generateToolsAction();
				plugin.loadFeature();
			}
		});
	}

	$.fn[singleViewerName] = function(options) {
		return this.each(function(){
			if (!$.data(this, singleViewerName)) {
				$.data(this, singleViewerName, new SingleSequenceViewer(this, options));
			}
		});
	}
	/*
	 * /SINGLE SEQUENCE VIEWER
	 * -------------------------------------------------------------------
	 */

	/*
	 * MULTIPLE SEQUENCE VIEWER
	 */
	var multipleViewerName = "multipleSequenceViewer";
	function MultipleSequenceViewer(element, options) {
		this._name = multipleViewerName;
		this.element = element;
		this.options = $.extend({}, defaults, options);
		this.options.colors = $.extend({}, defaults.colors, options.colors);
		this._defaults = defaults;
		this._defaults.infoClick = function(){}
		this.seqs = [];
		this.seqsBackup = [];
		this.oldMaxLength = 0;
		this.seqWidth = 30;
    this.highlightPosition = {
    	from:-1,
    	to:-1
    }
    this.proteinMode = false;
    this.firstSearch = true;
    this.currentRow = 0;
    this.currentIndex = 0;
    this.viewerTemplate = tempOpen.concat(
    						toolbar.concat(
    						midBar.concat(
    						contentBar.concat(
    					  tempClose))));
    this.viewerTemplate = this.viewerTemplate.join("");
    this.sequenceIndexInput = null;
    this.rowIndexInput = null;
		this.init();
		this.ready = true;
	}

	/*
   * Reset state of the viewer
   */
	MultipleSequenceViewer.prototype.resetState = function(){
		var svResetState = SingleSequenceViewer.prototype.resetState.bind(this);
		svResetState();
		this.checkEquality();
		this.notifyRowIndex(0);
	}

	/*
	 * Create container for the sequence viewer
	 * with default tools
	 */
	MultipleSequenceViewer.prototype.createContainer = function() {
		this.element.innerHTML = this.viewerTemplate;
		$(this.element).find("._sv_progress_container").hide();
		$(this.element).find("._svf_add_feature").remove();
		$(this.element).find("._sv_info_container").css({
			'min-height':'100px'
		});
		$(this.element).find("._sv_sequence_container").addClass("_sv_absolute_container");
		this.sequenceIndexInput = $(this.element).find("._sv_seq_index");
		this.rowIndexInput = $(this.element).find("._sv_row_index");
	}

	/*
	 * Multiple sequence fasta file parser
	 * fill the sequence object with the parsed fasta file content
	 */
	MultipleSequenceViewer.prototype.parseFastaFile = function(fasta) {
      this.seqs = parseFasta(fasta);
  }

  /*
   * create sequences info 
   * to be inserted at the specified container
   */
  function createSequenceInfo(seqs) {
  	var info = "",
  		result = "";
  	for (i=0; i<seqs.length; i++) {
  		info = seqs[i].info;
  		result += "<a href='#' class='_msv_seq_info'>" + info + "</a><br/>";
  	}
  	return result;
  }


  MultipleSequenceViewer.prototype.loadSequenceInfo = function() {
    var $infoContainer = $(this.element).find("._sv_info_container").css("padding", 0);
  	var originalWidth = $infoContainer.width();
		$infoContainer.html("<div class='_msv_sequence_info'>" + createSequenceInfo(this.seqs) + "</div>")
    			  		  .width(originalWidth);
    var $sequenceInfo = $(this.element).find("._msv_sequence_info");
    $sequenceInfo.css("max-width", $sequenceInfo.parent().width());
    $(this.element).find("._msv_sequence_info a:last").css("margin-bottom", 5);
  }

  /*
	 * Load sequence data to the container
	 */
	MultipleSequenceViewer.prototype.loadSequenceData = function() {
		colorListMultiple = this.options.colors;
		var seqColorMultiple = function(d) {
	    	if (colorListMultiple[d]) return colorListMultiple[d];
	    	return "#fff";
	  }

		var $container = $(this.element).find("._sv_sequence_container, ._sv_progress_container");
		$container.width($container.parent().width());
		$container.height($container.parent().height());
		$(this.element).find("._sv_progress_container").show();

		var container = $container.get(0);
		this.dataContainer = d3.select(container).text("")
			.append("svg")
			.attr("width", (this.seqWidth * this.seqs.maxLength)+"px")
			.attr("height", this.seqWidth * this.seqs.length);
	    
    var seq = null;
    var seqWidth = this.seqWidth;
		var plugin = this;
		var i = 0;

		function loadByRow() {
			var percent = Math.round((i+1)/plugin.seqs.length*100);
			plugin.notifyProgress(percent);
			// sequence box
	  	plugin.dataContainer.selectAll("._sv_seq_row_" + i)
				.data(seq)
				.enter()
				.append("rect")
				  .attr("class", function(d,j){ return "_sv_seq_row_" + i +" _sv_seq_" + j; })
	              .attr("x", function(d,j){ return j * seqWidth; })
	              .attr("y", i*plugin.seqWidth)
	              .attr("width", plugin.seqWidth)
	              .attr("height", plugin.seqWidth)
	              .style("fill", function(d,i){ return seqColorMultiple(d); })
	              .attr("stroke", "#000");

	    // sequence label
			plugin.dataContainer.selectAll("._sv_seq_label_" + i)
	      .data(seq)
	      .enter()
	      .append("text")
					.attr("class", "_sv_seq_label_" + i + " _msv_label")
					.attr("transform", function(d,j) {
						return "translate ("+((j*seqWidth)+10)+", " + ((i*seqWidth)+18) + ")";
					})
					.each(function(d,i){
						d3.select(this).text(d);
					})

	    // sequence index
			plugin.dataContainer.selectAll("._sv_seq_index_" + i)
	      .data(seq)
	      .enter()
	        .append("text")
	          .attr("class", "_sv_seq_index_" + i + " _sv_seq_index")
	          .attr("transform", function(d,j) {
	          	return "translate ("+((j*seqWidth)+3)+", " + ((i*seqWidth)+26) + ")";
	          })
	          .each(function(d,i){
	            d3.select(this).text(i+1);
	          })
		}
		function asyncLoad() {
			setTimeout(function(){
				seq = plugin.seqs[i].data;
				loadByRow();
				i++;
				if (i<plugin.seqs.length) {
					asyncLoad();
				} else {
					$(plugin.element).find("._sv_translator").removeAttr("disabled");
					plugin.ready = true;
				}
			}, 10);
		}
		asyncLoad();
	}

	/*
	 * Check for one columns full sequence equality
	 */
	MultipleSequenceViewer.prototype.checkEquality = function() {
		var seq = null;
		var equality = [];
    for (i=0; i<this.seqs.length; i++) {
    	seq = this.seqs[i].data;
	    for (j=0; j<seq.length; j++) {
	    	if (!equality[j]) {
	    		equality[j] = {
	    			type : seq[j],
	    			count : 1
	    		};
	    	} else if (equality[j].type != seq[j]) {
	    		equality[j].type = null;
	    	} else {
	    		equality[j].count++;
	    	}
	    }
		}
		var $equalityContainer = $(this.element).find("._msv_equality_container").html("");
		$equalityContainer.width($equalityContainer.parent().width());
		var container = $equalityContainer.get(0);
		this.equalityContainer = d3.select(container)
				.append("svg")
				.attr("width", this.dataContainer.attr("width"))
				.attr("height", 25);

	 	var seqWidth = this.seqWidth;
		// equality box
  	this.equalityContainer.selectAll("._msv_equality_box")
			.data(equality)
			.enter()
			.append("rect")
			  .attr("class", "_msv_equality_box")
		          .attr("x", function(d,i){ return i * seqWidth; })
		          .attr("y", 0)
		          .attr("width", seqWidth)
		          .attr("height", seqWidth)
		          .attr("fill", "#eee")
		          .attr("stroke", "#ddd");

	 	// equality labels
		this.equalityContainer.selectAll("._msv_equality_label")
      .data(equality)
      .enter()
      .append("text")
				.attr("class", "_msv_equality_label")
				.attr("transform", function(d,j) {
					return "translate ("+((j*seqWidth)+13)+", 23)";
				})
				.each(function(d,i){
					var txt = "";
					if (d.type != null && d.count > 1) txt = "*";
					d3.select(this).text(txt);
				})
	}

  /*
   * Get current sequence index from the view
   */
  MultipleSequenceViewer.prototype.getCurrentSequenceIndex = SingleSequenceViewer.prototype.getCurrentSequenceIndex;

	/*
	 * Go to specified index
	 */
	MultipleSequenceViewer.prototype.goTo = SingleSequenceViewer.prototype.goTo;

	/*
   * Set the viewer status
   */
	MultipleSequenceViewer.prototype.notify = SingleSequenceViewer.prototype.notify; 

	/*
   * Notify the current sequence index
   */
	MultipleSequenceViewer.prototype.notifySequenceIndex = function(index) {
    this.sequenceIndexInput.val(index);
    this.rowIndexInput.val(this.currentRow+1);
	}

  /*
   * Notify the current row index
   */
	MultipleSequenceViewer.prototype.notifyRowIndex = function(index) {
		var infoList = $(this.element).find("._msv_seq_info");
		var old = infoList[this.currentRow];
    $(old).css("background-color","")
    	  .css("color", $(old).data("oldColor"))
    	  .data("active", false);

		this.currentRow = index;
    var elm = infoList[index];
    $(elm).data("oldColor", $(elm).css("color"))
        .css({
        	"background-color":"#999",
        	"color":"white"
        })
        .data("active", true);
    $(this.element).find("._sv_row_index").val(index+1);
  }

  /*
   * Notify progress of some process
   */
  MultipleSequenceViewer.prototype.notifyProgress = function(percent){
  	var plugin = this;
  	if (percent == 100) {
  		$(plugin.element).find("._sv_progress_percent").html("100%");
  		$(plugin.element).find("._sv_progress_container").fadeOut(2000, function(){
  			$(plugin.element).find("._sv_progress_percent").html("0%");
  		});
  		return;
  	}
  	$(this.element).find("._sv_progress_percent").html(percent+"%");
  }

	/*
	 * Motif Search Utility
	 */
	MultipleSequenceViewer.prototype.motifSearch = function(query) {
		var seq = this.seqs[this.currentRow];
		var q = query.toUpperCase();
		q = translateSearchQuery(q); // translate query into RegExp
		if (this.firstSearch) {
			this.firstSearch = false;
		} else {
			q.lastIndex = this.getCurrentSequenceIndex();
		}

		var plugin = this;
		var notFound = function(){
			plugin.resetHighlight();
			plugin.notify("<i>Search not found for '" + query + "'</i>");
			return false;
		}

		var arraySearch = function(rgx, data, from, until) {
			var result = -1;
			var current = from;
			try {
				do {
					result = rgx.exec(data[current].data);
					if (result == -1 || result == null) {
						rgx.lastIndex = 0;
						current++;
					}
				} while ((result == -1 || result == null) && current != until);
			} catch(e) {
				return -1;
			}
			return {
				result:result,
				lastRow:current
			};
		};

		var search = arraySearch(q, this.seqs, this.currentRow, this.currentRow-1);
		var result = search.result;

		// search decission
		if (result == -1 || result == null) { // Not Found
			q.lastIndex = 0; // Reset to first index of the sequence
			search = arraySearch(q, this.seqs, 0, this.seqs.length-1);
			result = search.result;
			if (result == -1 || result == null) {
				notFound();
				return false;
			}
		}

		// Search found
		q.lastIndex = search.result.index + search.result[0].length;
		this.notifyRowIndex(search.lastRow);
		this.highlight(result.index, result.index + result[0].length);
		this.notify("<i>Found at "+ (result.index+1) + " </i>");
		$(this.element).find("._sv_sequence_container").scrollLeft(search.result.index * 30);
		this.notifySequenceIndex(search.result.index+1);
	}

	/*
	 * Highlight the sequence at specified index
	 */
	MultipleSequenceViewer.prototype.highlight = function(from, to) {
		var container = $(this.dataContainer[0][0]);
		var highlighter = "";
		// Reset highlight
		if (this.highlightPosition.from != -1 || this.highlightPosition.to != -1
			|| this.highlightPosition.row != -1) {
			this.resetHighlight();
		}
		for (i=from; i<to; i++) {
			var seq = $(this.element)
					  .find("._sv_seq_row_" + this.currentRow + "._sv_seq_"+i);
			seq.get(0).oldFill = seq.css("fill");
			seq.attr("stroke", this.options.highlightColor)
			   .css("fill", this.options.highlightColor);
		}
		// Remember the highligthed index
		this.highlightPosition.from = from;
		this.highlightPosition.to = to;
		this.highlightPosition.row = this.currentRow;
	}

	/*
	 * Reset sequence highlight
	 */
	MultipleSequenceViewer.prototype.resetHighlight = function() {
		for (i=this.highlightPosition.from; i<this.highlightPosition.to; i++) {
			var seq = $(this.element)
					  .find("._sv_seq_row_" + this.highlightPosition.row + "._sv_seq_"+i);
			seq.attr("stroke", "black")
				.css("fill", seq.get(0).oldFill);
		}
		this.highlightPosition.from = -1;
		this.highlightPosition.to = -1;
		this.highlightPosition.row = -1;
	}

	/*
	 * Load sequence data to the container
	 */
	MultipleSequenceViewer.prototype.generateToolsAction = function() {
		// Backup the plugin context
		var plugin = this;

		// Responsive feature
		var oldMultipleWidth = $(window).width();
		$(window).resize(function(){
			if (oldMultipleWidth == $(window).width()) return;
			var $container = $(plugin.element).find("._sv_sequence_container, ._sv_progress_container");
			$container.width($container.parent().width());
			$container.height($container.parent().height());

			//var $equalityContainer = $(plugin.element).find("._msv_equality_container");//
			//$equalityContainer.width($equalityContainer.parent().width());

			var $infoContainer = $(plugin.element).find("._msv_sequence_info");
			$infoContainer.width($infoContainer.parent().width());
			plugin.checkEquality();
		}); 

		// paired scroll action
		$(this.element).find("._msv_equality_container").scroll(function(){
			var offset = $(this).scrollLeft();
			$(plugin.element).find("._sv_sequence_container")
							 .scrollLeft(offset);
		});
		$(this.element).find("._sv_sequence_container").scroll(function(){
			var offset = $(this).scrollLeft();
			$(plugin.element).find("._msv_equality_container")
							 .scrollLeft(offset);
		});

		// Active sequence index
		var onActiveSequenceIndexChanged = function(){
			var index = plugin.getCurrentSequenceIndex();
			plugin.firstSearch = false;
			plugin.goTo(index-1);
		};
		$(this.element).find("._sv_seq_index").keyup(onActiveSequenceIndexChanged)
	   																			.change(onActiveSequenceIndexChanged);

    // Search box
		$(this.element).find("._sv_search_box").keyup(function(e){
			if ($(this).val() == "") {
				plugin.notify("");
				plugin.resetHighlight();
				return;
			}
			if (e.keyCode == 13) {
				plugin.motifSearch($(this).val());
			}
		});

		// Active sequence index
		var onActiveRowIndexChanged = function(){
			var index = $(this).val();
			if (isNaN(index) || index > plugin.seqs.length || index == "") return false;
			plugin.notifyRowIndex(index-1);
		};
		$(this.element).find("._sv_row_index").keyup(onActiveRowIndexChanged)
						   														.change(onActiveRowIndexChanged);

		// Sequence info
		$(this.element).find("._msv_seq_info").each(function(rowIndex, info){
			$(info).click(function(){
				if ($(this).data("active")) {
					if (plugin.options.infoClick) plugin.options.infoClick(this);
				} else {
					plugin.notifyRowIndex(rowIndex);
				}
				return false;
			});
		});

		// Sequence info search
		$(this.element).find("._msv_info_box").keyup(function(){
			var val = $(this).val();
			for (i=0; i<plugin.seqs.length; i++) {
				if (plugin.seqs[i].info.indexOf(val) != -1) {
					plugin.notifyRowIndex(i);
					break;
				}
			}
		});

		// Sequence translation
		$(this.element).find("._sv_translator").click(function(){
			var btnTranslateContext = this;
			if (!plugin.ready) return false;
      $(this).attr("disabled", "disabled");
			plugin.ready = false;
			if (plugin.proteinMode) {
				var i = 0;
				function toNucleotideByRow(i) {
					plugin.seqs[i].data = plugin.seqsBackup[i];
					plugin.seqs.maxLength = plugin.oldMaxLength;
				}
				function asyncLoadNucleotide() {
					setTimeout(function(){
						toNucleotideByRow(i);
						i++;
						if (i<plugin.seqs.length) {
							asyncLoadNucleotide();
						} else {
							plugin.resetState();
							$(btnTranslateContext).html("Show translated protein form")
								   									.addClass("btn-primary")
								   									.removeClass("btn-info");
							//$(btnTranslateContext).removeAttr("disabled");
							plugin.proteinMode = !plugin.proteinMode;
							//plugin.ready = true;
						}
					}, 10);
				}
				asyncLoadNucleotide();
			} else {
				plugin.oldMaxLength = plugin.seqs.maxLength;
				plugin.seqs.maxLength = 0;
				// Ansyncronious protein translation
				var i =0;
				function toProteinByRow(i) {
					plugin.seqsBackup[i] = plugin.seqs[i].data;
					plugin.seqs[i].data = translateToProtein(plugin.seqs[i].data);
					if (plugin.seqs[i].data.length > plugin.seqs.maxLength) {
						plugin.seqs.maxLength = plugin.seqs[i].data.length;
					} 
				}
				function asyncLoad() {
					setTimeout(function(){
						toProteinByRow(i);
						i++;
						if (i<plugin.seqs.length) {
							asyncLoad();
						} else {
							var $infoContainer = $(plugin.element).find("._msv_sequence_info");
							$infoContainer.css("max-width", $infoContainer.parent().width());	
							plugin.resetState();
							$(btnTranslateContext).html("Show nucleotide form")
														   		  .addClass("btn-info")
														        .removeClass("btn-primary");
							//$(btnTranslateContext).removeAttr("disabled");
							plugin.proteinMode = !plugin.proteinMode;
							//plugin.ready = true;
						}
					}, 10);
				}
				asyncLoad();
			}
		});

		// Sequence alignment
		$(this.element).find("._sv_alignment").click(function(){
			var btnAlignment = $(this);
			var fastaFile = plugin.options["alignment"];
			if (!fastaFile) return false;
			plugin.proteinMode = false;
			plugin.notify("<font color='blue'>Fetching alignment source</font>");
			$.ajax({
				"url":fastaFile,
				"dataType":"text",
				success:function(fasta){
					plugin.createContainer();
					$(plugin.element).find("._sv_alignment").remove();
					plugin.notify("<font color='#000088'>Parsing alignment source</font>");
					plugin.parseFastaFile(fasta);
					plugin.loadSequenceInfo();
					plugin.loadSequenceData();
					plugin.checkEquality();
					plugin.notifyRowIndex(plugin.currentRow);
					plugin.generateToolsAction();
					plugin.notify("<font color='green'>Alignment success</font>", 2000);
				},
				error:function(){
					plugin.notify("<font color='red'>Alignment failed, source not found</font>");
					setTimeout(function(){
						$(plugin.element).find("._sv_status").fadeOut(1000, function(){
							plugin.notify("");
							$(this).show();
						});
					}, 5000);
				}
			});
		});
	}

	MultipleSequenceViewer.prototype.init = function(){
		var fastaFile = this.options["fasta"];
		if (!fastaFile) return this.element;

		var plugin = this;
		plugin.createContainer();
		$.ajax({
			"url":fastaFile,
			"dataType":"text",
			success:function(fasta){
				plugin.parseFastaFile(fasta);
				plugin.loadSequenceInfo();
				plugin.loadSequenceData();
				plugin.checkEquality();
				plugin.notifyRowIndex(plugin.currentRow);
				plugin.generateToolsAction();
			}
		});
	}
	
	$.fn[multipleViewerName] = function(options) {
		return this.each(function(){
			if (!$.data(this, multipleViewerName)) {
				$.data(this, multipleViewerName, new MultipleSequenceViewer(this, options));
			}
		});
	}
})(jQuery, window, document)